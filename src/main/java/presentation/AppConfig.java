package presentation;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * https://docs.spring.io/spring/docs/current/javadoc-api/org/springframework/context/annotation/Configuration.html
 * via component scanning <br>
 * autowiring semantics
 */
@EnableWebMvc
@Configuration
@ComponentScan(basePackages = "presentation") 
public class AppConfig {

	private final SomeBean someBean;

	public AppConfig(SomeBean someBean) {
		this.someBean = someBean;
	}

	// @Bean definition using "SomeBean"

}
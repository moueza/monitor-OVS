package presentation;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SomeBean {
	int bean1;

	public int getBean1() {
		return bean1;
	}

	@Bean
	public void setBean1(int bean1) {
		this.bean1 = bean1;
	}

}
